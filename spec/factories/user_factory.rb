FactoryGirl.define do
  sequence(:email) { |n| "test#{n}@example.com" }
  factory :user do
    email
    password 'password'
    password_confirmation 'password'

    trait :admin do
      admin true
    end
  end
end
